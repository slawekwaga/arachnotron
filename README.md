# Arachnotron

Arachnotron is a launcher for Doom source ports such as GZDoom developed in QT with cross platform in mind that supports user made game profiles.


# Development Setup

QT Creator is used to develop this app, making use of its QML designer.

This app requires at least QT 5.10 (currently using QT 5.12) releases of QT with QT Creator (IDE) can be found here: http://download.qt.io/official_releases/qt/

On Linux installing QT Creator via a package manager may not include a recent enough version of QT. You could install QT Creator via a package manager and then use the releases link above to download and install a newer QT GCC, you can then go to Manage Kits in QT Creator, then the QT Versions tab, click add and locate bin/qmake as a to create a manual kit in QT Creator, you can then head to the Kits tab and change the QT Version to the new version you added. This may be cleaner as you can later remove the manual installation once your package manager provides a recent enough QT version.

You may also need to install the qtdeclarative5-dev package.

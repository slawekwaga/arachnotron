QT += quick
CONFIG += c++11

DEFINES += QT_DEPRECATED_WARNINGS

TARGET = arachnotron
TEMPLATE = app

DESTDIR = ../bin
MOC_DIR = ../build/moc
RCC_DIR = ../build/rcc
UI_DIR = ../build/ui
unix:OBJECTS_DIR = ../build/o/unix
win32:OBJECTS_DIR = ../build/o/win32
macx:OBJECTS_DIR = ../build/o/mac

SOURCES += \
    src/main.cpp \
    src/jsonloader.cpp \
    src/settingsmanager.cpp \
    src/categorymodel.cpp \
    src/categorymanager.cpp \
    src/profilemodel.cpp \
    src/profilemanager.cpp \
    src/profilelaunch.cpp \
    src/profilebase.cpp

HEADERS += \
    include/jsonloader.h \
    include/settingsmanager.h \
    include/categorymanager.h \
    include/categorymodel.h \
    include/profilemodel.h \
    include/profilemanager.h \
    include/profilelaunch.h \
    include/profilebase.h

DISTFILES += \
    qml/main.qml \
    qml/settings.qml \
    qml/engines.qml \
    qml/iwads.qml \
    qml/categoryButton.qml \
    qml/categoryEdit.qml \
    qml/categoryEditControls.qml \
    qml/categoryView.qml \
    qml/categoryViewControls.qml \
    qml/profileButton.qml \
    qml/profileEdit.qml \
    qml/profileEditControls.qml \
    qml/profileView.qml \
    qml/profileViewControls.qml \
    qml/list.qml \
    qml/listEntryText.qml \
    qml/listEntryDropdown.qml \
    qml/listEntryFile.qml \
    qml/listEntryEngine.qml \
    qml/listEntryIwad.qml \
    qml/listEntryCvarProfile.qml \
    qml/listEntryCvarLaunch.qml \
    qml/Arachnotron/arachnotron.qml \
    scripts/main.js \
    scripts/workspace.js \
    scripts/controls.js \
    scripts/editor.js \
    scripts/settings.js \
    scripts/engines.js \
    scripts/iwads.js \
    scripts/categories.js \
    scripts/categoryView.js \
    scripts/profiles.js \
    scripts/popupWindow.js \
    scripts/dialogWindow.js \
    scripts/fileLoader.js \
    scripts/listLoader.js \
    scripts/list.js \
    scripts/listEntry.js

RESOURCES += \
    qml.qrc

win32 {
    RC_FILE = icon.rc
}

QML_IMPORT_PATH += \
    qml

QML_DESIGNER_IMPORT_PATH += \
    qml

qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

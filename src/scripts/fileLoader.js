.pragma library

// This script handles the main file dialog. (Better performance with just one).
var settingsManager;

var fileDialog;
var fileAcceptedFunction;
var fileDialogTarget;
var fileRootpath;

// Settings Manager:
function setSettingsManager(manager) {
    settingsManager = manager;
}

// Set File Dialog:
function setFileDialog(target) {
    fileDialog = target;
}

// Gets the file loader prefix for paths.
function getLoaderPrefix() {
    var loaderPrefix = "file://";
    if(settingsManager.isWindows()) {
        loaderPrefix += "/";
    }
    return loaderPrefix;
}

// Opens a file dialog.
function openFileDialog(title, acceptedFunction, target, startFolder, save, folder, multi, rootPath) {
    fileDialog.title = title;
    fileAcceptedFunction = acceptedFunction;
    fileDialogTarget = target;
    fileRootpath = rootPath;
    if(startFolder) {
        this.fileDialog.folder = getLoaderPrefix() + startFolder;
    }
    fileDialog.selectExisting = save !== true;
    fileDialog.selectFolder = folder === true;
    fileDialog.selectMultiple = multi === true;
    fileDialog.open();
}

// Called when the File Dialog has selected a file.
function fileDialogAccepted() {
    if(!fileAcceptedFunction) {
        return;
    }
    this.fileAcceptedFunction(fileDialogTarget, getCleanUrls(fileDialog.fileUrls, fileRootpath));
}

// Cleans up file urls.
function getCleanUrls(inputUrls, rootPath) {
    var loaderPrefix = getLoaderPrefix();
    var urls = [];
    for(var i in inputUrls) {
        urls[i] = inputUrls[i].replace(loaderPrefix, "");
        if(rootPath) {
            urls[i] = urls[i].replace(rootPath, "");
        }
    }
    return urls;
}

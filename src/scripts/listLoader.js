.pragma library

function loadList(formQml, target) {
    // No Form:
    if(!formQml) {
        return null;
    }

    // Create New Form:
    var component = Qt.createComponent(formQml);
    var list = component.createObject(target);
    if(!list) {
        console.log("Error creating list: " + formQml);
        return undefined;
    }
    list.anchors.top = list.parent.top;
    list.anchors.bottom = list.parent.bottom;
    list.anchors.left = list.parent.left;
    list.anchors.right = list.parent.right;

    return list;
}

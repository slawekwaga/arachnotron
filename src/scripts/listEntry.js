.import "settings.js" as Settings
.import "profiles.js" as Profiles

var entryIndex;

function loadEntry(entry, list, index) {
    entry.list = list;
    entryIndex = index;
    if(!index) {
        return;
    }
    entry.inputFields[0].text = index;
}

function loadDropdownEntry(entry, list, index) {
    entry.list = list;
    entryIndex = index;
    entry.setComboTarget("value", entry.inputFields[0], index, list.modeData);
}

function loadCvarProfileEntry(entry, list, index) {
    entry.list = list;
    entryIndex = index;
    if(!index) {
        return;
    }
    entry.inputFields[0].text = index;
    if(!list.sourceInherited)
        entry.inputFields[1].text = list.source.getMapValue(key, index);
    else
        entry.inputFields[1].text = list.source.getInheritedMapValue(key, index);
}

function loadCvarLaunchEntry(entry, list, index) {
    entry.list = list;
    entryIndex = index;
    if(!index) {
        return;
    }
    entry.name.text = index;
    if(Profiles.getLaunch() && Profiles.getLaunch().getMapValue(key, index))
        entry.inputFields[0].text = Profiles.getLaunch().getMapValue(key, index);
    else {
        if(!list.sourceInherited)
            entry.inputFields[0].text = list.source.getMapValue(key, index);
        else
            entry.inputFields[0].text = list.source.getInheritedMapValue(key, index);
    }
}

function loadEngineEntry(entry, list, index) {
    entry.list = list;
    entryIndex = index;
    if(!index) {
        return;
    }
    if(typeof index !== "string") {
        entry.inputFields[1].text = index[0];
        return;
    }
    entry.inputFields[0].text = index;
    entry.inputFields[1].text = Settings.settingsManager.getEnginePath(index);
    entry.inputFields[2].text = Settings.settingsManager.getEngineConfig(index);
}

function loadIwadEntry(entry, list, index) {
    entry.list = list;
    entryIndex = index;
    if(!index) {
        return;
    }
    if(typeof index !== "string") {
        entry.inputFields[1].text = index[0];
        return;
    }
    entry.inputFields[0].text = index;
    entry.inputFields[1].text = Settings.settingsManager.getIwad(index);
}


// Controls:
function removeEntry(list) {}

function removeCvarLaunchEntry(list) {
    if(!entryIndex) {
        return;
    }
    Profiles.getLaunch().removeMapValue(list.key, entryIndex);
}

function resetCvarLaunchEntry(target) {
    if(Settings.selectedProfileLaunch)
        Settings.selectedProfileLaunch.removeMapValue("cvars", target.name.text);
    if(Profiles.selectedProfile) {
        if(!list.sourceInherited)
            target.inputFields[0].text = list.source.getMapValue("cvars", target.name.text);
        else
            target.inputFields[0].text = list.source.getInheritedMapValue("cvars", target.name.text);
    }
}


// Paths:
function setPath(target, urls) {
    target.text = urls[0];
}

var pathTarget;
function setPathTarget(target) {
    pathTarget = target;
}

var configTarget;
function setConfigTarget(target) {
    configTarget = target;
}

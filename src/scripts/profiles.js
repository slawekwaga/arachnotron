.pragma library
.import "workspace.js" as Workspace
.import "controls.js" as Controls
.import "categories.js" as Categories

// This script manages profiles as a whole.
var profileManager;
var selectedProfile = null;
var profileViewer = null;
var profileEditor = null;

var hideJoinInput = false;


// Profile Manager:
function setProfileManager(manager) {
    profileManager = manager;
}


// ========== Profiles ==========
// Add Profile:
function addProfile() {
    selectedProfile = null;
    profileEditor = Workspace.setWorkspace("../qml/profileEdit.qml");
    Controls.setControls("../qml/profileEditControls.qml");
    if(profileEditor) {
        profileEditor.title.text = "Create New Profile";
    }
}

// Remove Profile:
function removeProfile() {
    Workspace.setWorkspace("");
    Controls.setControls("");

    if(selectedProfile) {
        profileManager.removeProfile(selectedProfile.getId());
        selectedProfile = null;
        profileManager.writeToJson();
    }

    Categories.selectCategory(Categories.selectedCategoryButton);
}

// Select Profile:
function selectProfile(profileButton) {
    Categories.reselectCategory = true;
    if(profileButton) {
        selectedProfile = profileManager.getProfile(profileButton.profileId);
    }
    if(!selectedProfile) {
        console.log("Tried to view a null profile, cancelling.");
        return;
    }
    profileViewer = Workspace.setWorkspace("../qml/profileView.qml");
    Controls.setControls("../qml/profileViewControls.qml");
}

// Edit Profile:
function editProfile() {
    Categories.reselectCategory = true;
    profileEditor = Workspace.setWorkspace("../qml/profileEdit.qml");
    Controls.setControls("../qml/profileEditControls.qml");
}

// Save Profile:
function saveProfile() {
    // New Profile:
    if(!selectedProfile) {
        selectedProfile = profileManager.createProfile();
    }

    // Apply Save Data:
    if(profileEditor) {
        profileEditor.saveData(selectedProfile);
        selectedProfile.writeToJson();
        profileManager.writeToJson();
    }

    selectProfile(false);
}

// Quick Launch:
function quickLaunch(profileButton) {
    if(profileButton) {
        var profile = profileManager.getProfile(profileButton.profileId);
        if(profile)
            profile.launch();
    }
}

// Quick Edit:
function quickEdit(profileButton) {
    if(profileButton) {
        selectedProfile = profileManager.getProfile(profileButton.profileId);
        if(selectedProfile)
            editProfile();
    }
}

// Import:
function onImportProfile(importedProfile) {
    if(!importedProfile)
        return;
    selectedProfile = importedProfile;
    selectProfile();
    Categories.deselectCategoryButton();
}


// ========== Launch Settings ==========
// Get Launch:
function getLaunch() {
    if(!selectedProfile)
        return false;
    return selectedProfile.getLaunchSettings();
}

// Save Launch Settings:
function saveLaunch() {
    if(!selectedProfile)
        return;

    var launchSettings = getLaunch();
    profileViewer.saveData(launchSettings);
    profileManager.writeToJson();
}

// Launch:
function launch() {
    saveLaunch();
    if(selectedProfile) {
        selectedProfile.launch();
    }
}

// Launch Preview:
function loadLaunchCommandPreview(target) {
    if(!selectedProfile) {
        return;
    }
    saveLaunch();
    target.text = selectedProfile.getLaunchCommand();
}

.import "fileLoader.js" as FileLoader
.import "listLoader.js" as ListLoader

var dataTargets = [];
var textTargets = [];
var doubleTargets = [];
var boolTargets = [];
var comboTargets = [];
var listTargets = [];
var mapTargets = [];


// Save Data:
function saveData(destination) {
    for(var key in textTargets) {
        var textValue = textTargets[key].text;
        if(key === "data_sourcePath") {
            destination.setSourcePath(textValue);
            continue;
        }
        if(key === "data_categories") {
            textValue = textValue.replace(", ", ",").split(",");
        }
        destination.setString(key.replace("data_", ""), textValue);
    }

    for(key in doubleTargets) {
        destination.setDouble(key.replace("data_", ""), doubleTargets[key].text);
    }

    for(key in boolTargets) {
        destination.setBool(key.replace("data_", ""), boolTargets[key].checked);
    }

    for(key in comboTargets) {
        destination.setString(key.replace("data_", ""), getComboValue(comboTargets[key]));
    }

    for(key in listTargets) {
        destination.setList(key.replace("data_", ""), listTargets[key].getSaveData());
    }

    for(key in mapTargets) {
        destination.clearMap(key.replace("data_", ""));
        var mapSaveData = mapTargets[key].getSaveData();
        for(var i in mapSaveData) {
            destination.addMapValue(key.replace("data_", ""), mapSaveData[i][0], mapSaveData[i][1]);
        }
    }
}


// Data Targets:
function setDataTarget(key, target) {
    dataTargets["data_" + key] = target;
}

function getDataTarget(key) {
    return dataTargets["data_" + key];
}

function setTextTarget(key, target, source) {
    textTargets["data_" + key] = target;
    if(source) {
        target.text = source.getString(key);
    }
}

function getTextTarget(key) {
    return textTargets["data_" + key];
}

function setDoubleTarget(key, target, source) {
    doubleTargets["data_" + key] = target;
    if(source) {
        target.text = source.getDouble(key);
    }
}

function getDoubleTarget(key) {
    return doubleTargets["data_" + key];
}

function setBoolTarget(key, target, source) {
    boolTargets["data_" + key] = target;
    if(source) {
        target.checked = source.getBool(key);
    }
}

function getBoolTarget(key) {
    return boolTargets["data_" + key];
}

function setComboTarget(key, target, source, values, names) {
    var entries = [];
    var index = 0;
    if(!names) {
        names = values;
    }
    for(var i in values) {
        entries[values[i]] = { "index": index++, "value": values[i], "name": names[i] };
    }
    for(var j in entries) {
        target.model.append(entries[j]);
    }
    if(source) {
        var currentValue;
        if(typeof source === "string") {
            currentValue = source;
        }
        else {
            currentValue = source.getString(key);
        }
        if(entries[currentValue]) {
            target.currentIndex = entries[currentValue]["index"];
        }
        else if(target.editable) {
            target.editText = currentValue;
        }
    }
    comboTargets["data_" + key] = target;
}

function getComboValue(target) {
    if(target.currentIndex < 0 || target.editable) {
        return target.editText;
    }

    if(target.currentIndex >= target.model.count) {
        return target.editText;
    }

    return target.model.get(target.currentIndex).value;
}

function updateEditableCombo(target) {
    var editText = target.editText;
    if(!isComboValue(target, target.editText)) {
        target.currentIndex = -1;
        target.editText = editText;
    }
}

function isComboValue(target, value) {
    for(var i = 0; i < target.model.count; i++) {
        if(target.model.get(i).value === value) {
            return true;
        }
    }
    return false;
}

function setListTarget(key, target, source, entryQML, mode, modeData, inherited) {
    var list = ListLoader.loadList("../qml/list.qml", target);
    list.loadFromList(list, key, source, entryQML, mode, modeData, inherited);
    listTargets["data_" + key] = list;
}

function setMapTarget(key, target, source, entryQML, mode, modeData, inherited) {
    var list = ListLoader.loadList("../qml/list.qml", target);
    list.loadFromMap(list, key, source, entryQML, mode, modeData, inherited);
    mapTargets["data_" + key] = list;
}


// Special Combos:
function setNetModeTarget(key, target, source) {
    setComboTarget(key, target, source,
                        [0, 1],
                        ["Peer To Peer", "Packet Server"]
    );
}

function setGameTypeTarget(key, target, source) {
    setComboTarget(key, target, source,
                        ["", "deathmatch", "altdeath"],
                        ["Coop", "Deathmatch", "Alt Deathmatch"]
    );
}


// Paths:
function setPath(target, urls) {
    target.text = urls[0];
}

function getRootPath(key) {
    var rootPath = getTextTarget(key).text;
    return rootPath.substring(0, rootPath.lastIndexOf('/')) + "/";
}

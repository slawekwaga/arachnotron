.pragma library
.import "settings.js" as Settings
.import "listLoader.js" as ListLoader
.import "fileLoader.js" as FileLoader

// This script is for iwads.
var iwadsList;
var listContainer;


// Save:
function saveSettings() {
    var saveData = iwadsList.getSaveData();
    Settings.settingsManager.clearIwads();
    for(var i in saveData) {
         Settings.settingsManager.addIwad(saveData[i][0], saveData[i][1]);
    }
    Settings.settingsManager.writeToJson();
}


// Lists:
function loadIwadsList(target) {
    listContainer = target;
    iwadsList = ListLoader.loadList("../qml/list.qml", target);
    iwadsList.entryQML = "../qml/listEntryIwad.qml";
    iwadsList.mode = "iwads";

    var iwads = Settings.settingsManager.getIwadKeys();
    for(var i in iwads) {
        var entry = iwadsList.addEntry(iwadsList, iwads[i]);
    }
}


// Find:
function findInFolder() {
    FileLoader.openFileDialog("Find Iwads In Folder", findInFolderUrl, undefined, undefined, false, true);
}

function findInFolderUrl(target, urls) {
    Settings.settingsManager.writeToJson();
    if(Settings.settingsManager.findIwads(urls[0])) {
        iwadsList.destroy();
        loadIwadsList(listContainer);
    }
}

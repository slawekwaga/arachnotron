.pragma library
.import "settings.js" as Settings
.import "popupWindow.js" as PopupWindow
.import "dialogWindow.js" as DialogWindow

// This script controls global launcher windows as well as the bottom left controls.

// Open Settings:
function openSettings() {
    PopupWindow.setPopupForm("../qml/settings.qml");
}


// Open Engines:
function openEngines() {
    PopupWindow.setPopupForm("../qml/engines.qml");
}


// Open IWADs:
function openIwads() {
    PopupWindow.setPopupForm("../qml/iwads.qml");
}

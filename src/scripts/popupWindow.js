.pragma library

// This script controls popups (large overlay windows).
// There can only be one popup active at a time.

// Popup Target:
var popupTarget = null;
function setPopupTarget(target) {
    popupTarget = target;
}

var popupBlur = null;
function setPopupBlur(target) {
    popupBlur = target;
}

var popupForm = null;
function setPopupForm(formQml) {
    // Remove Existing Form:
    if(popupForm !== null) {
        //if(popupForm.type !== undefined)
            popupForm.destroy();
        popupForm = null;
    }

    // No Form:
    if(formQml === "") {
        popupBlur.visible = false;
        popupBlur.enabled = false;
        return;
    }

    // Create New Form:
    var component = Qt.createComponent(formQml);
    popupForm = component.createObject(popupTarget);
    if(popupForm === null) {
        console.log("Error creating popup form: " + formQml);
        return;
    }
    popupForm.anchors.top = popupForm.parent.top;
    popupForm.anchors.bottom = popupForm.parent.bottom;
    popupForm.anchors.left = popupForm.parent.left;
    popupForm.anchors.right = popupForm.parent.right;
    popupBlur.visible = true;
    popupBlur.enabled = true;
}

import QtQuick 2.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/profiles.js" as Profiles
import "../scripts/fileLoader.js" as FileLoader

Item {
    width: 1024
    height: 32
    Row {
        id: editRow
        anchors.fill: parent ? parent : undefined
        layoutDirection: Qt.RightToLeft
        spacing: 4

        Button {
            id: saveButton
            height: 48
            text: qsTr("Save")
            rightPadding: 32
            leftPadding: 32
            font.capitalization: Font.MixedCase
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            highlighted: true
            font.bold: true
            font.family: Arachnotron.bigFont
            font.pointSize: 22
        }

        Button {
            id: resetButton
            height: 32
            text: "Reset"
            highlighted: true
            icon.source: "../assets/exportIcon.svg"
            anchors.bottom: parent.bottom
            font.pointSize: 12
            rightPadding: 16
            anchors.bottomMargin: 0
            leftPadding: 10
            font.family: Arachnotron.bigFont
            font.capitalization: Font.MixedCase
            font.bold: true
        }
    }

    Connections {
        target: saveButton
        onClicked: Profiles.saveProfile()
    }

    Connections {
        target: resetButton
        onClicked: Profiles.editProfile()
    }
}

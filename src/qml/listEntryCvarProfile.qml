import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Arachnotron 1.0
import "../scripts/listEntry.js" as ListEntry

Item {
    id: cvarEntry
    width: 416
    height: 32

    property var inputFields: [nameInput, valueInput]
    property var list: null
    property var load: ListEntry.loadCvarProfileEntry;
    property var remove: ListEntry.removeEntry;

    RowLayout {
        id: cvarLayout
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 10
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 10
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.bottomMargin: 1
        anchors.top: parent ? parent.top : undefined
        anchors.topMargin: 1

        TextField {
            id: nameInput
            text: qsTr("CVar")
            font.family: Arachnotron.mainFont
            font.pointSize: 12
            selectByMouse: true
            rightPadding: 4
            leftPadding: 4
            bottomPadding: 0
            topPadding: 3
            Layout.fillWidth: true
            Layout.preferredWidth: 150
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
        }

        TextField {
            id: valueInput
            text: qsTr("Default")
            font.family: Arachnotron.mainFont
            font.pointSize: 12
            selectByMouse: true
            rightPadding: 4
            leftPadding: 4
            bottomPadding: 0
            topPadding: 3
            Layout.fillWidth: true
            Layout.preferredWidth: 150
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
        }

        Button {
            id: upButton
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/upArrowIcon.svg"
        }

        Button {
            id: downButton
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/downArrowIcon.svg"
        }

        Button {
            id: removeButton
            text: qsTr("X")
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/closeIcon.svg"
        }
    }

    Connections {
        target: removeButton
        onClicked: {
            list.removeEntry(cvarEntry)
            ListEntry.removeEntry(cvarEntry)
        }
    }

    Connections {
        target: upButton
        onClicked: list.moveEntryUp(cvarEntry)
    }

    Connections {
        target: downButton
        onClicked: list.moveEntryDown(cvarEntry)
    }
}

import QtQuick 2.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/categories.js" as Categories
import "../scripts/profiles.js" as Profiles

Item {
    width: 1024
    height: 32

    Row {
        id: launchRow
        anchors.fill: parent ? parent : undefined
        layoutDirection: Qt.RightToLeft
        spacing: 4

        Button {
            id: launchButton
            height: 48
            text: qsTr("Add Profile")
            rightPadding: 32
            leftPadding: 32
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            highlighted: true
            font.capitalization: Font.MixedCase
            font.pointSize: 22
            font.family: Arachnotron.bigFont
        }

        Button {
            id: editButton
            height: 32
            text: "Edit Category"
            rightPadding: 16
            leftPadding: 12
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            highlighted: true
            font.family: Arachnotron.bigFont
            font.pointSize: 12
            icon.source: "../assets/editIcon.svg"
        }

        Button {
            id: refreshButton
            height: 32
            text: "Refresh"
            rightPadding: 16
            leftPadding: 12
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            highlighted: true
            font.family: Arachnotron.bigFont
            font.pointSize: 12
            icon.source: "../assets/exportIcon.svg"
        }

        Button {
            id: removeButton
            height: 32
            text: "Remove"
            highlighted: true
            icon.source: "../assets/removeIcon.svg"
            anchors.bottom: parent.bottom
            font.pointSize: 12
            rightPadding: 16
            anchors.bottomMargin: 0
            leftPadding: 12
            font.family: Arachnotron.bigFont
        }
    }

    Connections {
        target: launchButton
        onClicked: Profiles.addProfile()
    }

    Connections {
        target: editButton
        onClicked: Categories.editCategory()
    }

    Connections {
        target: refreshButton
        onClicked: Categories.selectCategory()
    }

    Connections {
        target: removeButton
        onClicked: Categories.removeCategory()
    }
}

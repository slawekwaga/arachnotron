import QtQuick 2.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/profiles.js" as Profiles

Item {
    width: 1024
    height: 32

    Row {
        id: launchRow
        anchors.fill: parent ? parent : undefined
        layoutDirection: Qt.RightToLeft
        spacing: 4

        Button {
            id: launchButton
            height: 48
            text: qsTr("Launch")
            rightPadding: 32
            leftPadding: 32
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            highlighted: true
            font.capitalization: Font.MixedCase
            font.pointSize: 22
            font.family: Arachnotron.bigFont
        }

        Button {
            id: editButton
            height: 32
            text: "Edit Profile"
            leftPadding: 10
            rightPadding: 16
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            font.capitalization: Font.MixedCase
            wheelEnabled: false
            highlighted: true
            font.family: Arachnotron.bigFont
            font.pointSize: 12
            icon.source: "../assets/editIcon.svg"
        }

        Button {
            id: saveButton
            height: 32
            text: "Save Defaults"
            highlighted: true
            icon.source: "../assets/importIcon.svg"
            anchors.bottom: parent.bottom
            wheelEnabled: false
            font.pointSize: 12
            rightPadding: 16
            anchors.bottomMargin: 0
            leftPadding: 10
            font.family: Arachnotron.bigFont
            font.capitalization: Font.MixedCase
        }

        Button {
            id: refreshButton
            height: 32
            text: "Refresh"
            highlighted: true
            icon.source: "../assets/exportIcon.svg"
            anchors.bottom: parent.bottom
            wheelEnabled: false
            font.pointSize: 12
            rightPadding: 16
            anchors.bottomMargin: 0
            leftPadding: 10
            font.family: Arachnotron.bigFont
            font.capitalization: Font.MixedCase
        }

        Button {
            id: removeButton
            height: 32
            text: "Remove"
            rightPadding: 16
            leftPadding: 12
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            highlighted: true
            font.family: Arachnotron.bigFont
            font.pointSize: 12
            icon.source: "../assets/removeIcon.svg"
        }
    }

    Connections {
        target: launchButton
        onClicked: Profiles.launch()
    }

    Connections {
        target: editButton
        onClicked: Profiles.editProfile()
    }

    Connections {
        target: saveButton
        onClicked: Profiles.saveLaunch()
    }

    Connections {
        target: refreshButton
        onClicked: Profiles.selectProfile()
    }

    Connections {
        target: removeButton
        onClicked: Profiles.removeProfile()
    }
}

import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Arachnotron 1.0
import "../scripts/fileLoader.js" as FileLoader
import "../scripts/listEntry.js" as ListEntry
import "../scripts/editor.js" as Editor

Item {
    id: listEntry
    width: 416
    height: 48

    property var inputFields: [valueInput]
    property var rootDirectoryInput: null
    property var list: null
    property var load: ListEntry.loadDropdownEntry
    property var remove: ListEntry.removeEntry;
    property var setComboTarget: Editor.setComboTarget;

    RowLayout {
        id: fileLayout
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 10
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 10
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.bottomMargin: 1
        anchors.top: parent ? parent.top : undefined
        anchors.topMargin: 1

        ComboBox {
            id: valueInput
            height: 40
            font.pointSize: 16
            Layout.fillWidth: true
            Layout.preferredWidth: 204
            currentIndex: -1
            textRole: "name"
            model: ListModel {
                id: valueInputItems
            }
            font.family: Arachnotron.mainFont
            editable: true
            contentItem: TextField {
                text: valueInput.editText
                bottomPadding: 0
                topPadding: 0
                font.pointSize: 12
                selectByMouse: true
            }
        }

        Button {
            id: upButton
            padding: 12
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 40
            Layout.preferredHeight: 40
            Layout.maximumWidth: 40
            Layout.minimumWidth: 40
            Layout.minimumHeight: 40
            Layout.maximumHeight: 40
            icon.source: "../assets/upArrowIcon.svg"
        }

        Button {
            id: downButton
            padding: 12
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 40
            Layout.preferredHeight: 40
            Layout.maximumWidth: 40
            Layout.minimumWidth: 40
            Layout.minimumHeight: 40
            Layout.maximumHeight: 40
            icon.source: "../assets/downArrowIcon.svg"
        }

        Button {
            id: removeButton
            padding: 12
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 40
            Layout.preferredHeight: 40
            Layout.maximumWidth: 40
            Layout.minimumWidth: 40
            Layout.minimumHeight: 40
            Layout.maximumHeight: 40
            icon.source: "../assets/closeIcon.svg"
        }
    }

    Connections {
        target: removeButton
        onClicked: {
            list.removeEntry(listEntry)
            ListEntry.removeEntry(listEntry)
        }
    }

    Connections {
        target: upButton
        onClicked: list.moveEntryUp(listEntry)
    }

    Connections {
        target: downButton
        onClicked: list.moveEntryDown(listEntry)
    }
}

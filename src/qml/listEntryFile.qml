import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import Arachnotron 1.0
import "../scripts/fileLoader.js" as FileLoader
import "../scripts/listEntry.js" as ListEntry

Item {
    id: fileEntry
    width: 416
    height: 32

    property var inputFields: [valueInput]
    property var rootDirectoryInput: null
    property var list: null
    property var load: ListEntry.loadEntry
    property var remove: ListEntry.removeEntry;

    RowLayout {
        id: fileLayout
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 10
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 10
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.bottomMargin: 1
        anchors.top: parent ? parent.top : undefined
        anchors.topMargin: 1

        TextField {
            id: valueInput
            text: qsTr("Path")
            font.family: "Verdana"
            font.pointSize: 10
            selectByMouse: true
            leftPadding: 8
            topPadding: 1
            bottomPadding: 0
            rightPadding: 4
            Layout.fillWidth: true
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27

            Component.onCompleted: ListEntry.setPathTarget(this)
        }

        Button {
            id: fileButton
            font.pointSize: 16
            font.family: Arachnotron.mainFont
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.preferredWidth: 27
            Layout.minimumWidth: 27
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumWidth: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/fileIcon.svg"
        }

        Button {
            id: folderButton
            font.pointSize: 16
            font.family: Arachnotron.mainFont
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            Layout.preferredWidth: 27
            Layout.minimumWidth: 27
            Layout.preferredHeight: 27
            Layout.minimumHeight: 27
            Layout.maximumWidth: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/folderIcon.svg"
        }

        Button {
            id: upButton
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/upArrowIcon.svg"
        }

        Button {
            id: downButton
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/downArrowIcon.svg"
        }

        Button {
            id: removeButton
            Layout.alignment: Qt.AlignRight | Qt.AlignVCenter
            font.family: Arachnotron.mainFont
            font.pointSize: 16
            Layout.preferredWidth: 27
            Layout.preferredHeight: 27
            Layout.maximumWidth: 27
            Layout.minimumWidth: 27
            Layout.minimumHeight: 27
            Layout.maximumHeight: 27
            icon.source: "../assets/closeIcon.svg"
        }
    }

    Connections {
        target: removeButton
        onClicked: {
            list.removeEntry(fileEntry)
            ListEntry.removeEntry(fileEntry)
        }
    }

    Connections {
        target: upButton
        onClicked: list.moveEntryUp(fileEntry)
    }

    Connections {
        target: downButton
        onClicked: list.moveEntryDown(fileEntry)
    }

    Connections {
        target: fileButton
        onClicked: FileLoader.openFileDialog("Add Resource File", ListEntry.setPath, valueInput, valueInput.text)
    }

    Connections {
        target: folderButton
        onClicked: FileLoader.openFileDialog("Add Resource Folder", ListEntry.setPath, valueInput, valueInput.text, false, true)
    }
}

pragma Singleton
import QtQuick 2.0

Item {
    property string mainFont: mainFontLoader.name
    property string bigFont: bigFontLoader.name

    FontLoader {
        id: mainFontLoader
        source: "./Mercy.ttf"
    }

    FontLoader {
        id: bigFontLoader
        source: "./DooM.otf"
    }
}

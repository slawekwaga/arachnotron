import QtQuick 2.0
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/categories.js" as Categories
import "../scripts/categoryView.js" as CategoryView

Item {
    id: element
    width: 888
    height: 480

    Text {
        id: categoryTitle
        color: "#ffffff"
        text: Categories.selectedCategory.getString("name")
        anchors.top: parent ? parent.top : undefined
        anchors.topMargin: 0
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 20
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 32
        font.pixelSize: 40
        font.family: Arachnotron.bigFont
    }

    ScrollView {
        id: profilesView
        anchors.top: categoryTitle.bottom
        anchors.right: parent ? parent.right : undefined
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.left: parent ? parent.left : undefined
        anchors.topMargin: 16
        contentWidth: this.width
        clip: true
        ScrollBar.horizontal.active: false

        Flow {
            id: profilesLayout
            width: parent.width - 40
            x: 20
            spacing: 16

            Component.onCompleted: CategoryView.setProfileListTarget(this)
        }
    }
}

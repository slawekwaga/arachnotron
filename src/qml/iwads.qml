import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import Arachnotron 1.0
import "../scripts/popupWindow.js" as PopupWindow
import "../scripts/iwads.js" as Iwads

Item {
    id: iwads
    visible: true
    width: 1024
    height: 560

    Image {
        id: popupWindow
        anchors.rightMargin: 60
        anchors.leftMargin: 60
        anchors.bottomMargin: 60
        anchors.topMargin: 60
        anchors.fill: parent
        fillMode: Image.Tile
        source: "../assets/IwadsBG.png"


        DropShadow {
            id: contentShadow
            x: 0
            y: 0
            color: "#000000"
            radius: 8
            anchors.fill: contentRect
            samples: 17
            horizontalOffset: 2
            transparentBorder: true
            verticalOffset: 2
            source: contentRect
        }

        Rectangle {
            id: contentRect
            color: "#00000000"
            border.color: "#00000000"
            anchors.fill: parent

            Text {
                id: popupTitle
                x: 0
                y: 8
                color: "#ffffff"
                text: qsTr("IWADs")
                anchors.top: parent.top
                anchors.topMargin: 8
                font.pointSize: 16
                font.family: Arachnotron.bigFont
                styleColor: "#00000000"
                horizontalAlignment: Text.AlignHCenter
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
            }

            ColumnLayout {
                id: iwadsLayout
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 56
                anchors.top: popupTitle.bottom
                anchors.topMargin: 16
                anchors.rightMargin: 16
                anchors.leftMargin: 16
                anchors.right: parent.right
                anchors.left: parent.left
                Layout.fillWidth: true
                spacing: 8


                RowLayout {
                    id: headersLayout
                    Layout.fillWidth: true

                    Text {
                        id: namesLabel
                        color: "#ffffff"
                        text: qsTr("Name")
                        lineHeight: 1.22
                        font.pointSize: 16
                        verticalAlignment: Text.AlignBottom
                        font.family: Arachnotron.mainFont
                        Layout.fillWidth: true
                        horizontalAlignment: Text.AlignHCenter
                    }

                    Text {
                        id: pathsLabel
                        color: "#ffffff"
                        text: qsTr("Path")
                        lineHeight: 1.22
                        font.pointSize: 16
                        verticalAlignment: Text.AlignBottom
                        font.family: Arachnotron.mainFont
                        horizontalAlignment: Text.AlignHCenter
                        Layout.fillWidth: true
                    }
                }
                Rectangle {
                    id: enginesBackground
                    color: "#40000000"
                    Layout.fillHeight: true
                    Layout.fillWidth: true
                    border.color: "#00000000"

                    Component.onCompleted: Iwads.loadIwadsList(this)
                }
            }

            Row {
                id: buttonsRow
                layoutDirection: Qt.RightToLeft
                spacing: 4
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
                height: 48

                Button {
                    id: closeButton
                    text: qsTr("Close")
                    anchors.bottom: parent.bottom
                    anchors.top: parent.top
                    font.family: Arachnotron.bigFont
                    highlighted: true
                    font.bold: false
                    font.pointSize: 16
                }

                Button {
                    id: findButton
                    text: qsTr("Find In Folder")
                    anchors.bottom: parent.bottom
                    anchors.top: parent.top
                    font.family: Arachnotron.bigFont
                    highlighted: true
                    font.bold: false
                    font.pointSize: 16
                }
            }
        }
    }

    Connections {
        target: closeButton
        onClicked: {
            Iwads.saveSettings();
            PopupWindow.setPopupForm("");
        }
    }

    Connections {
        target: findButton
        onClicked: {
            Iwads.findInFolder();
        }
    }
}

/*##^##
Designer {
    D{i:11;anchors_y:400}D{i:12;anchors_y:400}
}
##^##*/

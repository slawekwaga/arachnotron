import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import Arachnotron 1.0
import "../scripts/categories.js" as Categories
import "../scripts/profiles.js" as Profiles
import "../scripts/settings.js" as Settings
import "../scripts/fileLoader.js" as FileLoader
import "../scripts/editor.js" as Editor

Item {
    id: element
    width: 900
    height: 1024

    property var title: editTitle
    property var saveData: Editor.saveData
    property var importField: sourceInput

    Text {
        id: editTitle
        x: 20
        color: "#ffffff"
        text: qsTr("Edit Profile")
        font.family: Arachnotron.bigFont
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        font.pixelSize: 32
    }

    Image {
        id: panelBackground
        anchors.top: editTitle.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 16
        source: "../assets/ProfileEditBG.png"
        fillMode: Image.Tile

        DropShadow {
            id: contentShadow
            x: 0
            y: 0
            color: "#000000"
            radius: 8
            source: content
            anchors.fill: content
            transparentBorder: true
            horizontalOffset: 2
            samples: 17
            verticalOffset: 2
        }

        ScrollView {
            id: content
            bottomPadding: 20
            topPadding: 20
            anchors.fill: parent
            contentWidth: this.width
            clip: true
            ScrollBar.horizontal.active: false

            Flow {
                id: contentLayout
                width: parent.width - 40
                x: 20
                spacing: 16

                ColumnLayout {
                    id: infoLayout
                    width: {
                        var newWidth = (parent.width / 2) - (parent.spacing / 2);
                        if(newWidth >= 416)
                            return newWidth;
                        return parent.width;
                    }
                    spacing: 16

                    ColumnLayout {
                        id: descriptionLayout
                        Layout.fillWidth: true
                        spacing: 8

                        RowLayout {
                            id: nameLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: nameLabel
                                height: 27
                                color: "#ffffff"
                                text: qsTr("Profile Name")
                                Layout.preferredWidth: 204
                                font.pointSize: 16
                                verticalAlignment: Text.AlignBottom
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            TextField {
                                id: nameInput
                                height: 27
                                text: "New Profile"
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                padding: -1
                                rightPadding: 4
                                leftPadding: 4
                                bottomPadding: 0
                                topPadding: 3
                                font.pointSize: 16
                                font.family: Arachnotron.mainFont
                                selectByMouse: true
                                Component.onCompleted: Editor.setTextTarget("name", this, Profiles.selectedProfile)
                            }
                        }

                        RowLayout {
                            id: iwadLayout
                            Layout.fillWidth: true
                            spacing: 8

                            Text {
                                id: iwadLabel
                                color: "#ffffff"
                                text: qsTr("IWAD")
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            ComboBox {
                                id: iwadInput
                                height: 27
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                currentIndex: 0
                                font.pointSize: 16
                                textRole: "name"
                                model: ListModel {
                                    id: iwadItems
                                }
                                font.family: Arachnotron.mainFont
                                editable: true
                                Component.onCompleted: Editor.setComboTarget( "iwad", this, Profiles.selectedProfile, Settings.settingsManager.getIwadKeys())
                            }
                        }

                        RowLayout {
                            id: engineLayout
                            Layout.fillWidth: true
                            spacing: 8

                            Text {
                                id: engineLabel
                                color: "#ffffff"
                                text: qsTr("Engine")
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            ComboBox {
                                id: engineInput
                                height: 27
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                currentIndex: 0
                                font.pointSize: 16
                                textRole: "name"
                                model: ListModel {
                                    id: engineItems
                                }
                                font.family: Arachnotron.mainFont
                                editable: true
                                Component.onCompleted: Editor.setComboTarget( "engine", this, Profiles.selectedProfile, Settings.settingsManager.getEngineKeys())
                            }
                        }

                        RowLayout {
                            id: sourceLayout
                            Layout.fillWidth: true
                            spacing: 8

                            Text {
                                id: sourceLabel
                                color: "#ffffff"
                                text: qsTr("Profile Save Location")
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            TextField {
                                id: sourceInput
                                height: 27
                                text: Profiles.selectedProfile ? Profiles.selectedProfile.getSourcePath() : ""
                                Layout.fillWidth: true
                                Layout.preferredWidth: 156
                                font.family: "Verdana"
                                font.pointSize: 12
                                padding: -1
                                leftPadding: 8
                                rightPadding: 4
                                topPadding: 1
                                bottomPadding: 0
                                selectByMouse: true
                                Component.onCompleted: Editor.setTextTarget("sourcePath", this)
                            }

                            Button {
                                id: sourceButton
                                width: 40
                                height: 40
                                Layout.maximumHeight: 40
                                Layout.minimumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.minimumWidth: 40
                                font.pointSize: 16
                                icon.source: "../assets/saveIcon.svg"
                            }
                        }

                        RowLayout {
                            id: iconLayout
                            Layout.fillWidth: true
                            spacing: 8
                            Text {
                                id: iconLabel
                                color: "#ffffff"
                                text: qsTr("Icon Location")
                                font.pointSize: 16
                                verticalAlignment: Text.AlignBottom
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                                Layout.preferredWidth: 204
                            }

                            TextField {
                                id: iconInput
                                height: 27
                                text: ""
                                Layout.preferredWidth: 156
                                font.pointSize: 12
                                bottomPadding: 0
                                Layout.fillWidth: true
                                selectByMouse: true
                                leftPadding: 8
                                topPadding: 1
                                font.family: "Verdana"
                                padding: -1
                                rightPadding: 4
                                Component.onCompleted: Editor.setTextTarget("iconPath", this, Profiles.selectedProfile)
                            }

                            Button {
                                id: iconButton
                                width: 40
                                height: 40
                                font.pointSize: 16
                                Layout.minimumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.minimumWidth: 40
                                Layout.maximumHeight: 40
                                icon.source: "../assets/fileIcon.svg"
                            }
                        }

                        RowLayout {
                            id: backgroundLayout
                            Layout.fillWidth: true
                            spacing: 8
                            Text {
                                id: backgroundLabel
                                color: "#ffffff"
                                text: qsTr("Background Location")
                                font.pointSize: 16
                                verticalAlignment: Text.AlignBottom
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                                Layout.preferredWidth: 204
                            }

                            TextField {
                                id: backgroundInput
                                height: 27
                                text: ""
                                Layout.preferredWidth: 156
                                font.pointSize: 12
                                bottomPadding: 0
                                Layout.fillWidth: true
                                selectByMouse: true
                                leftPadding: 8
                                topPadding: 1
                                font.family: "Verdana"
                                padding: -1
                                rightPadding: 4
                                Component.onCompleted: Editor.setTextTarget("backgroundPath", this, Profiles.selectedProfile)
                            }

                            Button {
                                id: backgroundButton
                                width: 40
                                height: 40
                                font.pointSize: 16
                                Layout.minimumHeight: 40
                                Layout.maximumWidth: 40
                                Layout.minimumWidth: 40
                                Layout.maximumHeight: 40
                                icon.source: "../assets/fileIcon.svg"
                            }
                        }

                        RowLayout {
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: deathmatchWeaponsLabel
                                color: "#ffffff"
                                text: qsTr("Hide Tile Background")
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: deathmatchWeaponsInput
                                height: 27
                                checked: false
                                Component.onCompleted: Editor.setBoolTarget("backgroundTileHide", this, Profiles.selectedProfile)
                            }
                        }


                    }

                    ColumnLayout {
                        id: categoriesLayout
                        spacing: 8
                        width: parent.width
                        Layout.fillWidth: true

                        Text {
                            id: categoriesLabel
                            color: "#ffffff"
                            text: qsTr("Categories")
                            Layout.fillWidth: true
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Rectangle {
                            id: categoriesBackground
                            height: 390
                            color: "#40000000"
                            Layout.fillWidth: true
                            border.color: "#00000000"
                            Component.onCompleted: Editor.setListTarget("categories", this, Profiles.selectedProfile, "../qml/listEntryText.qml")
                        }
                    }

                }


                ColumnLayout {
                    id: dataLayout
                    width: infoLayout.width
                    spacing: 8

                    ColumnLayout {
                        id: inheritProfilesLayout
                        spacing: 8
                        width: parent.width
                        Layout.fillWidth: true

                        Text {
                            id: inheritProfilesLabel
                            color: "#ffffff"
                            text: qsTr("Inherit From Profiles")
                            Layout.fillWidth: true
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Rectangle {
                            id: inheritProfilesBackground
                            height: 190
                            color: "#40000000"
                            Layout.fillWidth: true
                            border.color: "#00000000"
                            Component.onCompleted: Editor.setListTarget("inheritProfiles", this, Profiles.selectedProfile, "../qml/listEntryDropdown.qml", "", Profiles.profileManager.getProfileNames())
                        }
                    }

                    ColumnLayout {
                        id: filesLayout
                        Layout.fillWidth: true
                        spacing: 8

                        Text {
                            id: filesLabel
                            color: "#ffffff"
                            text: qsTr("Profile Resources")
                            font.family: Arachnotron.mainFont
                            font.pointSize: 16
                            Layout.fillWidth: true
                            verticalAlignment: Text.AlignBottom
                            horizontalAlignment: Text.AlignHCenter
                            lineHeight: 1.22
                        }

                        Rectangle {
                            id: filesBackground
                            height: 390
                            color: "#40000000"
                            Layout.fillWidth: true
                            border.color: "#00000000"
                            Component.onCompleted: Editor.setListTarget("resources", this, Profiles.selectedProfile, "../qml/listEntryFile.qml", "files", sourceInput)
                        }
                    }


                    ColumnLayout {
                        id: playerClassesLayout
                        spacing: 8
                        width: parent.width
                        Layout.fillWidth: true

                        Text {
                            id: playerClassesLabel
                            color: "#ffffff"
                            text: qsTr("Player Classes")
                            Layout.fillWidth: true
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Rectangle {
                            id: playerClassesBackground
                            height: 190
                            color: "#40000000"
                            Layout.fillWidth: true
                            border.color: "#00000000"
                            Component.onCompleted: Editor.setListTarget("playerClasses", this, Profiles.selectedProfile, "../qml/listEntryText.qml")
                        }
                    }

                    ColumnLayout {
                        id: cvarsLayout
                        spacing: 8
                        width: parent.width
                        Layout.fillWidth: true

                        Text {
                            id: cvarsLabel
                            color: "#ffffff"
                            text: qsTr("Profile Console Variables")
                            Layout.fillWidth: true
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        Rectangle {
                            id: cvarsBackground
                            height: 190
                            color: "#40000000"
                            Layout.fillWidth: true
                            border.color: "#00000000"
                            Component.onCompleted: Editor.setMapTarget("cvars", this, Profiles.selectedProfile, "../qml/listEntryCvarProfile.qml")
                        }
                    }





                    ColumnLayout {
                        id: argsLayout
                        spacing: 8
                        width: parent.width
                        Layout.fillWidth: true

                        Text {
                            id: argsLabel
                            color: "#ffffff"
                            text: qsTr("Default Additional Arguments")
                            Layout.fillWidth: true
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        TextField {
                            id: argsInput
                            height: 27
                            text: ""
                            Layout.fillWidth: true
                            font.pointSize: 16
                            padding: -1
                            leftPadding: 4
                            topPadding: 3
                            bottomPadding: 0
                            font.family: Arachnotron.mainFont
                            rightPadding: 4
                            selectByMouse: true
                            Component.onCompleted: Editor.setTextTarget("args", this, Profiles.selectedProfile)
                        }
                    }
                }

            }
        }
    }

    Connections {
        target: sourceButton
        onClicked: FileLoader.openFileDialog("Profile Save Location", Editor.setPath, sourceInput, sourceInput.text, true)
    }

    Connections {
        target: iconButton
        onClicked: FileLoader.openFileDialog("Icon Image Location", Editor.setPath, iconInput, iconInput.text, false, false, false, Editor.getRootPath("sourcePath"))
    }

    Connections {
        target: backgroundButton
        onClicked: FileLoader.openFileDialog("Icon Image Location", Editor.setPath, backgroundInput, backgroundInput.text, false, false, false, Editor.getRootPath("sourcePath"))
    }
}

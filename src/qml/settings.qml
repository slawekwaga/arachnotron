import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0
import Arachnotron 1.0
import "../scripts/popupWindow.js" as PopupWindow
import "../scripts/editor.js" as Editor
import "../scripts/settings.js" as Settings

Item {
    property var saveData: Editor.saveData

    id: settings
    visible: true
    width: 1024
    height: 560
    Component.onCompleted: Settings.setSettingsView(this)

    Image {
        id: popupWindow
        anchors.rightMargin: 60
        anchors.leftMargin: 60
        anchors.bottomMargin: 60
        anchors.topMargin: 60
        anchors.fill: parent
        fillMode: Image.Tile
        source: "../assets/SettingsBG.png"


        DropShadow {
            id: contentShadow
            x: 0
            y: 0
            color: "#000000"
            radius: 8
            anchors.fill: contentRect
            samples: 17
            horizontalOffset: 2
            transparentBorder: true
            verticalOffset: 2
            source: contentRect
        }

        Rectangle {
            id: contentRect
            color: "#00000000"
            border.color: "#00000000"
            anchors.fill: parent

            Text {
                id: popupTitle
                x: 0
                y: 8
                color: "#ffffff"
                text: qsTr("Settings")
                anchors.top: parent.top
                anchors.topMargin: 8
                font.pointSize: 16
                font.family: Arachnotron.bigFont
                styleColor: "#00000000"
                horizontalAlignment: Text.AlignHCenter
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.left: parent.left
                anchors.leftMargin: 0
            }

            ScrollView {
                id: content
                x: 20
                y: 49
                anchors.top: popupTitle.bottom
                anchors.right: parent.right
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.rightMargin: 20
                anchors.leftMargin: 20
                anchors.bottomMargin: 60
                anchors.topMargin: 16

                Flow {
                    id: contentFlow
                    spacing: 16
                    anchors.fill: parent

                    Column {
                        id: systemColumn
                        width: 288
                        spacing: 8

                        Row {
                            id: autostartLayout
                            anchors.right: parent.right
                            anchors.rightMargin: 0
                            anchors.left: parent.left
                            anchors.leftMargin: 0
                            visible: false
                            Text {
                                id: autostartLabel
                                width: 214
                                height: 27
                                color: "#ffffff"
                                text: qsTr("Close On Launch")
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: autostartInput
                                height: 27
                            }
                            spacing: 8
                        }

                        Row {
                            id: minimizeCloseLayout
                            anchors.right: parent.right
                            anchors.rightMargin: 0
                            anchors.left: parent.left
                            anchors.leftMargin: 0
                            visible: false
                            Text {
                                id: minimizeCloseLabel
                                width: 214
                                height: 27
                                color: "#ffffff"
                                text: qsTr("Minimize To Tray")
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: minimizeCloseInput
                                height: 27
                            }
                            spacing: 8
                        }

                        Row {
                            id: minimizeTrayLayout
                            anchors.right: parent.right
                            anchors.rightMargin: 0
                            anchors.left: parent.left
                            anchors.leftMargin: 0
                            visible: false
                            Text {
                                id: minimizeTrayLabel
                                width: 214
                                height: 27
                                color: "#ffffff"
                                text: qsTr("Minimize On Close")
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: minimizeTrayInput
                                height: 27
                            }
                            spacing: 8
                        }

                        Row {
                            id: autocloseLayout
                            anchors.right: parent.right
                            anchors.rightMargin: 0
                            anchors.left: parent.left
                            anchors.leftMargin: 0
                            visible: false
                            Text {
                                id: autocloseLabel
                                width: 214
                                height: 27
                                color: "#ffffff"
                                text: qsTr("Open On System Start")
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: autocloseInput
                                height: 27
                            }
                            spacing: 8
                        }

                        Row {
                            id: noidleLayout
                            anchors.right: parent.right
                            anchors.rightMargin: 0
                            anchors.left: parent.left
                            anchors.leftMargin: 0
                            spacing: 8
                            Text {
                                id: noidleLabel
                                width: 214
                                height: 27
                                color: "#ffffff"
                                text: qsTr("No Idle (Windows)")
                                verticalAlignment: Text.AlignBottom
                                font.pointSize: 16
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            Switch {
                                id: noidleInput
                                height: 27
                                checked: true
                                Component.onCompleted: Editor.setBoolTarget("noidle", this, Settings.settingsManager)
                            }
                        }
                    }
                }
            }

            Button {
                id: closeButton
                x: 804
                y: 400
                text: qsTr("Close")
                font.family: Arachnotron.bigFont
                highlighted: true
                font.bold: false
                font.pointSize: 16
                anchors.right: parent.right
                anchors.rightMargin: 0
                anchors.bottom: parent.bottom
                anchors.bottomMargin: 0
            }
        }
    }

    Connections {
        target: closeButton
        onClicked: {
            Settings.saveSettings();
            PopupWindow.setPopupForm("");
        }
    }
}

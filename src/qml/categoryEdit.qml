import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Layouts 1.3
import QtGraphicalEffects 1.0
import Arachnotron 1.0
import "../scripts/categories.js" as Categories
import "../scripts/fileLoader.js" as FileLoader
import "../scripts/editor.js" as Editor

Item {
    id: element
    width: 900
    height: 480

    property var title: editTitle
    property var saveData: Editor.saveData
    property var importField: sourceInput

    Text {
        id: editTitle
        x: 20
        color: "#ffffff"
        text: qsTr("Edit Category")
        font.family: Arachnotron.bigFont
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors.right: parent.right
        anchors.rightMargin: 20
        anchors.left: parent.left
        anchors.leftMargin: 20
        font.pixelSize: 40
    }

    Image {
        id: panelBackground
        anchors.top: editTitle.bottom
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.topMargin: 16
        source: "../assets/CategoryEditBG.png"
        fillMode: Image.Tile

        DropShadow {
            id: contentShadow
            x: 0
            y: 0
            color: "#000000"
            radius: 8
            source: content
            anchors.fill: content
            transparentBorder: true
            horizontalOffset: 2
            samples: 17
            verticalOffset: 2
        }

        ScrollView {
            id: content
            bottomPadding: 20
            topPadding: 20
            anchors.fill: parent
            contentWidth: this.width
            clip: true
            ScrollBar.horizontal.active: false

            Flow {
                id: contentLayout
                width: parent.width - 40
                x: 20
                spacing: 16

                ColumnLayout {
                    id: infoLayout
                    width: {
                        var newWidth = (parent.width / 2) - (parent.spacing / 2);
                        if(newWidth >= 416) return newWidth;
                        return parent.width;
                    }
                    spacing: 16

                    ColumnLayout {
                        id: descriptionLayout
                        Layout.fillWidth: true
                        spacing: 8

                        RowLayout {
                            id: nameLayout
                            spacing: 8
                            width: parent.width
                            Layout.fillWidth: true

                            Text {
                                id: nameLabel
                                height: 27
                                color: "#ffffff"
                                text: qsTr("Category Name")
                                Layout.preferredWidth: 204
                                font.pointSize: 16
                                verticalAlignment: Text.AlignBottom
                                lineHeight: 1.22
                                font.family: Arachnotron.mainFont
                            }

                            TextField {
                                id: nameInput
                                height: 27
                                text: "New Category"
                                Layout.fillWidth: true
                                Layout.preferredWidth: 204
                                padding: -1
                                rightPadding: 4
                                leftPadding: 4
                                bottomPadding: 0
                                topPadding: 3
                                font.pointSize: 16
                                font.family: Arachnotron.mainFont
                                selectByMouse: true
                                Component.onCompleted: Editor.setTextTarget("name", this, Categories.selectedCategory)
                            }
                        }
                    }

                    RowLayout {
                        id: sourceLayout
                        Layout.fillWidth: true
                        spacing: 8

                        Text {
                            id: sourceLabel
                            color: "#ffffff"
                            text: qsTr("Category Save Location")
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        TextField {
                            id: sourceInput
                            height: 27
                            text: Categories.selectedCategory ? Categories.selectedCategory.getSourcePath() : ""
                            Layout.preferredWidth: 156
                            font.family: "Verdana"
                            font.pointSize: 12
                            Layout.fillWidth: true
                            padding: -1
                            leftPadding: 8
                            topPadding: 1
                            bottomPadding: 0
                            rightPadding: 4
                            selectByMouse: true
                            Component.onCompleted: Editor.setTextTarget("sourcePath", this)
                        }

                        Button {
                            id: sourceButton
                            width: 40
                            height: 40
                            Layout.maximumHeight: 40
                            Layout.minimumHeight: 40
                            Layout.maximumWidth: 40
                            Layout.minimumWidth: 40
                            font.pointSize: 16
                            icon.source: "../assets/saveIcon.svg"
                        }
                    }

                    RowLayout {
                        id: iconFileLayout
                        Layout.fillWidth: true
                        spacing: 8

                        Text {
                            id: iconLabel
                            color: "#ffffff"
                            text: qsTr("Icon Image Location")
                            Layout.preferredWidth: 204
                            verticalAlignment: Text.AlignBottom
                            font.pointSize: 16
                            lineHeight: 1.22
                            font.family: Arachnotron.mainFont
                        }

                        TextField {
                            id: iconInput
                            height: 27
                            text: ""
                            Layout.preferredWidth: 156
                            font.family: "Verdana"
                            font.pointSize: 12
                            Layout.fillWidth: true
                            padding: -1
                            leftPadding: 8
                            topPadding: 1
                            bottomPadding: 0
                            rightPadding: 4
                            selectByMouse: true
                            Component.onCompleted: Editor.setTextTarget("iconPath", this, Categories.selectedCategory)
                        }

                        Button {
                            id: iconButton
                            width: 40
                            height: 40
                            Layout.maximumHeight: 40
                            Layout.minimumHeight: 40
                            Layout.maximumWidth: 40
                            Layout.minimumWidth: 40
                            font.pointSize: 16
                            icon.source: "../assets/fileIcon.svg"
                        }
                    }
                }

                ColumnLayout {
                    id: dataLayout
                    width: {
                        var width = (parent.width / 2) - (parent.spacing / 2);
                        if(width >= 416) return width;
                        return parent.width;
                    }
                    spacing: 8
                }
            }
        }
    }

    Connections {
        target: sourceButton
        onClicked: FileLoader.openFileDialog("Category Save Location", Editor.setPath, sourceInput, sourceInput.text, true)
    }

    Connections {
        target: iconButton
        onClicked: FileLoader.openFileDialog("Icon Image Location", Editor.setPath, iconInput, iconInput.text, false, false, false, Editor.getRootPath("sourcePath"))
    }
}

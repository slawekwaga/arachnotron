import QtQuick 2.9
import QtQuick.Controls 2.2
import Arachnotron 1.0
import "../scripts/list.js" as List

Item {
    property var key: undefined
    property var source: undefined
    property var entryQML: undefined
    property var mode: undefined
    property var modeData: undefined
    property var resetFunction: undefined
    property bool sourceInherited: false

    property var listContainer: listContainer
    property var addButton: addButton
    property var resetButton: resetButton

    property var rootDirectoryInput // For file lists.

    property var loadFromList: List.loadFromList
    property var loadFromMap: List.loadFromMap
    property var loadEntries: List.loadEntries
    property var addEntry: List.addEntry
    property var removeEntry: List.removeEntry
    property var moveEntryUp: List.moveEntryUp
    property var moveEntryDown: List.moveEntryDown
    property var getSaveData: List.getSaveData

    id: list
    width: 416
    height: 190

    Rectangle {
        id: background
        color: "#00000000"
        anchors.bottomMargin: 4
        anchors.bottom: buttons.top
        anchors.right: parent ? parent.right : undefined
        anchors.left: parent ? parent.left : undefined
        anchors.top: parent ? parent.top : undefined
        border.color: "#00000000"

        ScrollView {
            id: scrollView
            contentWidth: parent.width
            bottomPadding: 10
            topPadding: 10
            clip: true
            anchors.fill: parent

            Column {
                id: listContainer
                spacing: 8
                anchors.right: parent.right
                anchors.left: parent.left
                anchors.top: parent.top
            }
        }
    }

    Text {
        id: count
        color: "#ffffff"
        text: qsTr("List Count: undefined")
        visible: false
        font.family: Arachnotron.mainFont
        font.pointSize: 16
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 20
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.bottomMargin: 8
    }

    Row {
        id: buttons
        layoutDirection: Qt.RightToLeft
        spacing: 8
        anchors.right: parent ? parent.right : undefined
        anchors.rightMargin: 0
        anchors.left: parent ? parent.left : undefined
        anchors.leftMargin: 0
        anchors.bottom: parent ? parent.bottom : undefined
        anchors.bottomMargin: 0

        Button {
            id: addButton
            width: 32
            height: 32
            text: ""
            font.pointSize: 12
            icon.source: "../assets/addIcon.svg"
        }

        Button {
            id: resetButton
            width: 32
            height: 32
            text: ""
            visible: false
            font.pointSize: 12
            icon.source: "../assets/importIcon.svg"
        }

    }

    DropArea {
        id: dropArea
        anchors.fill: parent
    }

    Connections {
        target: addButton
        onClicked: List.addEntry(list)
    }

    Connections {
        target: resetButton
        onClicked: List.reset(list)
    }

    Connections {
        target: dropArea
        onDropped: List.onFileDrop(list, drop.urls)
    }
}

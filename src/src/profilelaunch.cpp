#include "include/profilelaunch.h"
#include "include/profilemanager.h"

#include <QTextStream>

ProfileLaunch::ProfileLaunch(ProfileBase * profile) : ProfileBase() {
    this->profile = profile;
}

ProfileBase * ProfileLaunch::getProfile() {
    return this->profile;
}

QString ProfileLaunch::getModelType() {
    return "ProfileLaunch";
}

// JSON
bool ProfileLaunch::readFromJson() {
    // Load Profile Launch Settings:
    ProfileBase * profile = this->getProfile();
    QString profileName = profile->getString("name");
    if(!ProfileManager::instance->profileSettingsJSON.contains(profileName)) {
        return false;
    }
    QJsonObject json = ProfileManager::instance->profileSettingsJSON[profileName];

    this->fromJson(json);
    return true;
}

bool ProfileLaunch::writeToJson() {
    ProfileBase * profile = this->getProfile();
    QString profileName = profile->getString("name");
    QJsonObject settingsJson = this->toJson();
    ProfileManager::instance->profileSettingsJSON.insert(profileName, settingsJson);
    return true;
}

// Launch
QString ProfileLaunch::getLaunchArgs() {
    QString args;
    QTextStream out(&args);

    // Resources:
    foreach(QString resource, this->getList("resources")) {
        this->addArgQuotes(out, "-file", this->getPath(resource));
    }

    this->addArg(out, "+map", this->getString("map"));
    if(this->getString("map").compare("") != 0 || this->getBool("multiplayer"))
        this->addArg(out, "-skill", this->getString("skill"));
    this->addArg(out, "+playerclass", this->getString("playerClass"));
    this->addArgQuotes(out, "-loadgame", this->getString("loadGame"));
    this->addArg(out, "-nomonsters", this->getBool("noMonsters"));
    this->addArg(out, "-fast", this->getBool("fastMonsters"));
    this->addArg(out, "-respawn", this->getBool("respawnMonsters"));
    this->addArg(out, "+freeze", this->getBool("freeze"));
    this->addArg(out, "-timer", this->getDouble("timer"), 0);
    this->addArg(out, this->getString("args"));

    // Multiplayer:
    if(this->getBool("multiplayer")) {
        if(this->getBool("host")) {
            this->addArg(out, "-host", this->getDouble("players"));
            this->addArg(out, "-netmode", this->getDouble("netMode"));
            this->addArg(out, "-", this->getString("gameType"));
            this->addArg(out, "-dup", this->getDouble("dup"));
            this->addArg(out, "-extratic", this->getDouble("extraTics"));
            this->addArg(out, "+net_ticbalance 1", this->getBool("ticBalance"));
            this->addArg(out, "+sv_cheats 1", this->getBool("cheats"));
            this->addArg(out, "+sv_noweaponspawn 1", !this->getBool("deathmatchWeapons"));
        }
        else {
            this->addArg(out, "-join", this->getString("join"));
            // TODO Port
        }
    }

    return args;
}

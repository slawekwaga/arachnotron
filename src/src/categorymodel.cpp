#include "include/categorymodel.h"
#include "include/categorymanager.h"
#include "include/profilemanager.h"

#include <QDir>
#include <QTextStream>

// Constructor
CategoryModel::CategoryModel() : JsonLoader()
{
    CategoryManager::instance->qmlEngine->setObjectOwnership(this, QQmlEngine::CppOwnership);
    this->setString("name", "New Category");
    this->sourcePath = "";
}

QString CategoryModel::getModelType() {
    return "Category";
}

// Setters
void CategoryModel::setString(QString key, QString value) {
    if(key.compare("name") == 0) {
        for(ProfileModel * profile : this->getProfiles()) {
            profile->removeListValue("category", this->getString(key));
            profile->addListValue("category", key);
        }
    }
    this->JsonLoader::setString(key, value);
}

// Profiles
int CategoryModel::loadProfiles()
{
    this->profiles.clear();
    for(ProfileModel * profile : ProfileManager::instance->getProfiles()) {
        if(this->allProfiles || profile->isInCategory(this->getString("name"))) {
            this->profiles.append(profile);
        }
    }
    return this->profiles.size();
}

QVector<ProfileModel *> CategoryModel::getProfiles()
{
    return this->profiles;
}

ProfileModel * CategoryModel::getProfile(int index)
{
    return this->profiles[index];
}

#include "include/settingsmanager.h"
#include "include/categorymanager.h"
#include "include/profilemanager.h"

#include <QDebug>
#include <QFile>
#include <QDir>

SettingsManager * SettingsManager::instance = nullptr;

// Constructor
SettingsManager::SettingsManager(QQmlEngine * engine, QGuiApplication * app)
{
    engine->setObjectOwnership(this, QQmlEngine::CppOwnership);
    this->appName = app->applicationName();
    this->appVersion = app->applicationVersion();
    this->setSourcePath("config/settings.json");
}

// Initialize
bool SettingsManager::init()
{
    if(!readFromJson())
        writeToJson();
    SettingsManager::instance = this;
    return true;
}


// Info
QString SettingsManager::getAppName() {
    return this->appName;
}

QString SettingsManager::getAppVersion() {
    return this->appVersion;
}

bool SettingsManager::isWindows() {
    return QSysInfo::kernelType() == "winnt";
}


// JSON
void SettingsManager::parseJson(QString key, QJsonValue value) {
    // Engines:
    if(key.compare("engines") == 0) {
        QJsonArray enginesJson = value.toArray();
        foreach(const QJsonValue &entry, enginesJson) {
            QJsonObject entryJson = entry.toObject();
            this->addEngine(
                entryJson.value("name").toString(),
                entryJson.value("path").toString(),
                entryJson.value("config").toString()
            );
        }
        return;
    }

    // Iwads:
    if(key.compare("iwads") == 0) {
        QJsonArray iwadsJson = value.toArray();
        foreach(const QJsonValue &entry, iwadsJson) {
            QJsonObject entryJson = entry.toObject();
            this->addIwad(
                entryJson.value("name").toString(),
                entryJson.value("path").toString()
            );
        }
        return;
    }

    this->JsonLoader::parseJson(key, value);
}

bool SettingsManager::writeToJson() {
    QJsonObject settingsJson = this->toJson();

    // Engines:
    QJsonArray enginesJson;
    foreach(const QString &key, this->getEngineKeys()) {
        QJsonObject entryJson;
        QPair<QString, QString> engine = this->getEngine(key);
        entryJson.insert("name", key);
        entryJson.insert("path", engine.first);
        entryJson.insert("config", engine.second);
        enginesJson.append(entryJson);
    }
    settingsJson.insert("engines", enginesJson);

    // Iwads:
    QJsonArray iwadsJson;
    foreach(const QString &key, this->getIwadKeys()) {
        QJsonObject entryJson;
        entryJson.insert("name", key);
        entryJson.insert("path", this->getIwad(key));
        iwadsJson.append(entryJson);
    }
    settingsJson.insert("iwads", iwadsJson);

    return this->writeJsonToPath(this->getSourcePath(), settingsJson);
}


// ========== Import ==========
QObject * SettingsManager::importFile(QString path) {
    QJsonObject importJson = readJsonFromPath(path);

    // Profile:
    if(importJson.contains("categories") && importJson.contains("engine") && importJson.contains("iwad")) {
        qDebug() << "Importing Profile from: " << path;
        ProfileModel * profile = new ProfileModel();
        profile->setSourcePath(path);
        profile->readFromJson();
        ProfileManager::instance->addProfile(profile);
        ProfileManager::instance->writeToJson();
        return profile;
    }

    // Category:
    if(importJson.contains("name")) {
        qDebug() << "Importing Category from: " << path;
        CategoryModel * category = new CategoryModel();
        category->setSourcePath(path);
        category->readFromJson();
        CategoryManager::instance->addCategory(category);
        CategoryManager::instance->writeToJson();
        return category;
    }

    qDebug() << "Unable to detect Category or Profile data from: " << path;
    return nullptr;
}


// ========== Engines ==========
// Get Engine
QHash<QString, QPair<QString, QString>> SettingsManager::getEngines() {
    return this->engines;
}

// Get Engine Keys
QList<QString> SettingsManager::getEngineKeys() {
    return this->engines.keys();
}

// Clear Engines
void SettingsManager::clearEngines() {
    this->engines.clear();
}

// Get Engine
QPair<QString, QString> SettingsManager::getEngine(QString name) {
    if(!this->engines.contains(name))
        return QPair<QString, QString>("", "");
    return this->engines[name];
}

// Get Engine Path
QString SettingsManager::getEnginePath(QString name) {
    if(!this->engines.contains(name))
        return "";
    return this->engines[name].first;
}

// Get Engine Config
QString SettingsManager::getEngineConfig(QString name) {
    if(!this->engines.contains(name))
        return "";
    return this->engines[name].second;
}

// Add Engine
bool SettingsManager::addEngine(QString name, QString path, QString config) {
    if(this->engines.contains(name))
        return false;
    this->engines.insert(name, QPair<QString, QString>(path, config));
    return true;
}

// Remove Engine
bool SettingsManager::removeEngine(QString name) {
    if(!this->engines.contains(name))
        return false;
    this->engines.remove(name);
    return true;
}


// ========== Iwads ==========
// Find Iwads
bool SettingsManager::findIwads(QString path) {
    bool iwadFound = false;
    QDir dir(path);
    if(!dir.exists()) {
        qDebug() << "Invalid Iwad search directory: " << dir;
        return false;
    }
    for(QString filename : dir.entryList(QStringList() << "*.wad" << "*.WAD" << "*.iwad" << "*.IWAD")) {
        QString filePath = path + QDir::separator() + filename;
        if(filename.contains("Doom2", Qt::CaseInsensitive) || filename.contains("DoomBFG", Qt::CaseInsensitive)) {
            this->addIwad("Doom 2", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Doom", Qt::CaseInsensitive)) {
            this->addIwad("Doom", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Heretic", Qt::CaseInsensitive)) {
            this->addIwad("Heretic", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Hexen", Qt::CaseInsensitive)) {
            this->addIwad("Hexen", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Plutonia", Qt::CaseInsensitive)) {
            this->addIwad("Plutonia", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("TNT", Qt::CaseInsensitive) || filename.contains("Evilution", Qt::CaseInsensitive)) {
            this->addIwad("Evilution", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Strife", Qt::CaseInsensitive)) {
            this->addIwad("Strife", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Freedoom2", Qt::CaseInsensitive)) {
            this->addIwad("Freedoom 2", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Freedoom", Qt::CaseInsensitive)) {
            this->addIwad("Freedoom", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Blasphem", Qt::CaseInsensitive)) {
            this->addIwad("Blasphemer", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Zauberer", Qt::CaseInsensitive)) {
            this->addIwad("Zauberer", filePath);
            iwadFound = true;
            continue;
        }
        if(filename.contains("Harm", Qt::CaseInsensitive)) {
            this->addIwad("Harmony", filePath);
            iwadFound = true;
            continue;
        }
    }
    this->writeToJson();
    return iwadFound;
}

// Get Iwads
QHash<QString, QString> SettingsManager::getIwads() {
    return this->iwads;
}

// Get Iwad Keys
QList<QString> SettingsManager::getIwadKeys() {
    return this->iwads.keys();
}

// Clear Iwads
void SettingsManager::clearIwads() {
    this->iwads.clear();
}

// Get Iwad
QString SettingsManager::getIwad(QString name) {
    if(!this->iwads.contains(name))
        return "";
    return this->iwads[name];
}

// Add Iwad
bool SettingsManager::addIwad(QString name, QString path) {
    this->iwads[name] = path;
    return true;
}

// Remove Iwad
bool SettingsManager::removeIwad(QString name) {
    if(!this->iwads.contains(name))
        return false;
    this->iwads.remove(name);
    return true;
}

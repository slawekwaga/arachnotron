#include "include/profilemanager.h"

#include <QDir>
#include <QDebug>

ProfileManager * ProfileManager::instance = nullptr;

// Constructor
ProfileManager::ProfileManager(QQmlEngine * engine)
{
    this->qmlEngine = engine;
    engine->setObjectOwnership(this, QQmlEngine::CppOwnership);
    this->setSourcePath("config/profiles.json");
}

// Initialize
bool ProfileManager::init()
{
    ProfileManager::instance = this;
    return this->readFromJson();
}


// JSON
bool ProfileManager::readFromJson() {
    // Load Profile Settings:
    try {
        QJsonObject json = this->readJsonFromPath("config/profileSettings.json");
        QJsonArray profileSettingsJson = json.value("profileSettings").toArray();
        foreach(const QJsonValue &entry, profileSettingsJson) {
            QJsonObject entryJson = entry.toObject();
            QString profileName = entryJson.value("profile").toString();
            this->profileSettingsJSON.insert(profileName, entryJson.value("settings").toObject());
        }
    }
    catch(...) {}

    // Load Profiles:
    try {
        QJsonObject json = this->readJsonFromPath(this->getSourcePath());
        QJsonArray profilesJson = json.value("profiles").toArray();
        foreach(const QJsonValue &entry, profilesJson) {
            ProfileModel * profile = new ProfileModel();
            profile->setSourcePath(entry.toString());
            if(!profile->readFromJson()) {
                qDebug() << "Unable to read profile json: " << entry.toString();
                continue;
            }
            this->addProfile(profile);
        }
    }
    catch(...) {
        return false;
    }

    return true;
}

bool ProfileManager::writeToJson() {
    // Save Profile Settings:
    QJsonArray profileSettingsJson;
    foreach(ProfileModel * profile, this->getProfiles()) {
        profile->getLaunchSettings()->writeToJson();
        QString profileName = profile->getString("name");
        QJsonObject settingsJson = this->profileSettingsJSON[profileName];
        QJsonObject entryJson;
        entryJson.insert("profile", profileName);
        entryJson.insert("settings", settingsJson);
        profileSettingsJson.append(entryJson);
    }
    QJsonObject profileSettingsJsonObj;
    profileSettingsJsonObj.insert("profileSettings", profileSettingsJson);
    this->writeJsonToPath("config/profileSettings.json", profileSettingsJsonObj);

    // Save Profile Paths:
    QJsonArray profilesJson;
    foreach(const int profileId, this->getProfileIds()) {
        ProfileModel * profile = this->getProfile(profileId);
        if(profile->internal)
            continue;
        profilesJson.append(profile->getSourcePath());
    }
    QJsonObject profilesJsonObj;
    profilesJsonObj.insert("profiles", profilesJson);
    return this->writeJsonToPath(this->getSourcePath(), profilesJsonObj);
}

QString ProfileManager::getAbsolutePath(QString path) {
    if(QDir::isAbsolutePath(path))
        return path;
    return QFileInfo("profiles.json").absoluteDir().path() + QDir::separator() + path;
}


// ========== Profiles ==========
// Get Profiles
QMap<int, ProfileModel *> ProfileManager::getProfiles()
{
    return this->profiles;
}

// Get Profile IDs
QVector<int> ProfileManager::getProfileIds()
{
    return this->profileIds;
}

// Get Profile Names
QList<QString> ProfileManager::getProfileNames()
{
    return this->profilesByName.keys();
}

// Get Profile
ProfileModel * ProfileManager::getProfile(int profileId)
{
    if(!this->profiles.contains(profileId))
    {
        return nullptr;
    }
    return this->profiles[profileId];
}

ProfileModel * ProfileManager::getProfile(QString profileName)
{
    if(!this->profilesByName.contains(profileName))
    {
        return nullptr;
    }
    return this->profilesByName[profileName];
}

// Add Profile
bool ProfileManager::addProfile(ProfileModel * profile)
{
    if(profile->getId() >= 0)
    {
        if(this->profiles.contains(profile->getId()))
        {
            return false;
        }
    }
    else
    {
        profile->setId(this->nextId++);
    }
    this->profiles.insert(profile->getId(), profile);
    this->profilesByName.insert(profile->getString("name"), profile);
    this->profileIds.append(profile->getId());
    return true;
}

// Create Profile
ProfileModel * ProfileManager::createProfile()
{
    ProfileModel * profile = new ProfileModel();
    this->addProfile(profile);
    return profile;
}

// Import Profile
ProfileModel * ProfileManager::importProfile(QString path)
{
    ProfileModel * profile = new ProfileModel();
    profile->setSourcePath(path);
    profile->readFromJson();
    this->addProfile(profile);
    return profile;
}

// Remove Profile
bool ProfileManager::removeProfile(int profileId)
{
    if(!this->profiles.contains(profileId))
    {
        return false;
    }
    this->profilesByName.remove(this->profiles[profileId]->getString("name"));
    this->profiles.remove(profileId);
    this->profileIds.removeOne(profileId);
    return true;
}

// Clear Categories
void ProfileManager::clearProfiles()
{
    this->profiles.clear();
    this->profileIds.clear();
    this->profilesByName.clear();
}

#ifndef PROFILEMANAGER_H
#define PROFILEMANAGER_H

#include "include/profilemodel.h"

#include <QQmlEngine>

class ProfileManager : public JsonLoader {
    Q_OBJECT

protected:
    // A list of all loaded profiles.
    QMap<int, ProfileModel *> profiles;
    QMap<QString, ProfileModel *> profilesByName;

    // A list of valid profile IDs.
    QVector<int> profileIds;

    // The next available profile ID to use.
    int nextId = 0;

public:
    // Constructor
    ProfileManager(QQmlEngine * qmlEngine);

    // Initialize
    bool init();

    // Static Reference
    static ProfileManager * instance;

    // Engine
    QQmlEngine * qmlEngine;

    // Profile Settings JSON
    QHash<QString, QJsonObject> profileSettingsJSON;

public slots:
    // JSON
    virtual bool readFromJson() override;
    virtual bool writeToJson() override;
    QString getAbsolutePath(QString);

    // Profiles
    QMap<int, ProfileModel *> getProfiles();
    QVector<int> getProfileIds();
    QList<QString> getProfileNames();
    ProfileModel * getProfile(int);
    ProfileModel * getProfile(QString);
    bool addProfile(ProfileModel *);
    ProfileModel * createProfile();
    ProfileModel * importProfile(QString);
    bool removeProfile(int);
    void clearProfiles();
};

#endif

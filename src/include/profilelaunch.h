#ifndef PROFILELAUNCH_H
#define PROFILELAUNCH_H

#include "include/profilebase.h"

class ProfileLaunch : public ProfileBase {
    Q_OBJECT

protected:
    // The profile that these launch settings are for.
    ProfileBase * profile;

public:
    // Constructor
    ProfileLaunch(ProfileBase *);

    ProfileBase * getProfile();

public slots:
    virtual QString getModelType() override;

    // JSON
    virtual bool readFromJson() override;
    virtual bool writeToJson() override;

    // Launch
    QString getLaunchArgs();
};

#endif // PROFILELAUNCH_H

#ifndef JSONLOADER_H
#define JSONLOADER_H

#include <QString>
#include <QHash>
#include <QObject>
#include <QVector>
#include <QJsonObject>
#include <QJsonArray>
#include <QJsonValue>

class JsonLoader : public QObject {
    Q_OBJECT

public:
    // Constructor
    JsonLoader();

    // If true, this is an internal model (affect paths).
    bool internal = false;

    // JSON
    void fromJson(QJsonObject);
    virtual void parseJson(QString, QJsonValue);
    QJsonObject toJson();
    QJsonObject readJsonFromPath(QString);
    bool writeJsonToPath(QString, QJsonObject);
    QString readStringFromPath(QString);
    bool writeStringToPath(QString, QString);

protected:
    // The interface id of this model.
    int id;

    // The path where the json data for this model is located.
    QString sourcePath;

    // String values stored in this model.
    QHash<QString, QString> strings;

    // Double values stored in this model.
    QHash<QString, double> doubles;

    // Boolean values stored in this model.
    QHash<QString, bool> bools;

    // String lists stored in this model.
    QHash<QString, QVector<QString>> lists;

    // String maps stored in this model.
    QHash<QString, QHash<QString, QString>> maps;

public slots:
    virtual QString getModelType();

    // JSON
    virtual bool readFromJson();
    virtual bool writeToJson();

    // Getters
    int getId() const;
    QString getSourcePath() const;
    QString getSourceDir() const;
    QString getPath(QString) const;
    QString getAssetPath(QString) const;
    QString getString(QString) const;
    double getDouble(QString) const;
    bool getBool(QString) const;
    QVector<QString> getList(QString) const;
    QHash<QString, QString> getMap(QString) const;
    QString getMapValue(QString, QString) const;
    QList<QString> getMapKeys(QString) const;

    // Setters
    void setId(int);
    void setSourcePath(QString);
    virtual void setString(QString, QString);
    virtual void setDouble(QString, double);
    virtual void setBool(QString, bool);
    void setList(QString, QVector<QString>);

    // Lists
    void addListValue(QString, QString);
    bool removeListValue(QString, QString);

    // Maps
    void addMapValue(QString, QString, QString);
    bool removeMapValue(QString, QString);
    void clearMap(QString);
};

#endif // JSONLOADER_H
